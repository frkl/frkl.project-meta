[metadata]
name = frkl.project_meta
description = Manage and display metadata for a project created with the frkl.python-project template.
long_description = file: README.md
long_description_content_type = text/markdown
url = https://gitlab.com/frkl/frkl_project_meta
author = Markus Binsteiner
author_email = markus@frkl.io
license = "The Parity Public License 6.0.0"
license_file = LICENSE
platforms = any
classifiers =
    Development Status :: 3 - Alpha
    Programming Language :: Python
    Programming Language :: Python :: 3
    Programming Language :: Python :: 3 :: Only
    Programming Language :: Python :: 3.6
    Programming Language :: Python :: 3.7
    Programming Language :: Python :: 3.8
    Programming Language :: Python :: 3.9

[options]
packages = find_namespace:
install_requires =
    appdirs>=1.4.4,<2.0.0
    importlib_metadata;python_version<'3.8'
python_requires = >=3.6
include_package_data = True
package_dir =
    =src
setup_requires =
    setuptools_scm
    setuptools_scm_git_archive
zip_safe = False

[options.entry_points]
console_scripts =
    frkl-project = frkl.project_meta.interfaces.cli:cli
mkdocs.plugins =
    frkl-docgen = frkl.project_meta.documentation:FrklDocumentationPlugin

[options.extras_require]
cli =
    asyncclick>=7.0.9,<8.0.0
    uvloop>=0.14.0,<1.0.0;platform_system=="Linux"
dev_build =
    deepdiff>=5.2.0
    jinja2>=2.11.0
    pp-ez>=0.2.0
    pyinstaller>=4.2
dev_documentation =
    deepdiff>=5.2.0
    formic2>=1.0.3
    livereload>=2.6.3
    markdown-blockdiag>=0.7.0
    markdown-include>=0.6.0
    mkdocs-macros-plugin>=0.5.0
    mkdocs-material>=6.2.5
    mkdocs-simple-hooks>=0.1.2
    pip-licenses>=3.3.0
    pydoc-markdown>=3.9.0
    pymdown-extensions>=8.1
    watchgod>=0.6
dev_testing =
    flake8>=3.8.4
    mypy>=0.800
    pytest>=6.2.2
    pytest-cov>=2.11.1
    tox>=3.21.2
dev_utils =
    black
    cruft>=2.6.0
    flake8>=3.8.4
    importanize>=0.7.0
    ipython>=7.19.0
    pip-licenses>=3.3.0
    pp-ez>=0.2.0
    pre-commit>=2.9.3
    setup-cfg-fmt>=1.16.0
    watchgod>=0.6
    wheel
docs =
    mkdocs>=1.1.2

[options.packages.find]
where = src
exclude =
    tests

[aliases]
build = bdist_wheel
release = build upload

[bdist_wheel]
universal = 1

[devpi:upload]
no-vcs = 1
formats = sdist, bdist_wheel

[tool:pytest]
addopts =
    --verbose
norecursedirs =
    dist
    build
    .tox
testpaths = tests
pep8maxlinelength = 88

[tox:tox]
envlist = py36, py37, py38, py39, flake8

[testenv]
setenv =
    PYTHONPATH = {toxinidir}
deps =
    -e{toxinidir}[dev_testing,all]
install_command = pip install --pre --extra-index-url=https://pkgs.frkl.io/frkl/dev --extra-index-url=https://pkgs.frkl.dev/pypi {opts} {packages}
commands =
    pip install -U pip
    py.test --basetemp={envtmpdir}

[testenv:flake8]
basepython = python
deps =
    -e{toxinidir}[dev_testing,all]
    flake8
install_command = pip install --pre --extra-index-url=https://pkgs.frkl.io/frkl/dev --extra-index-url=https://pkgs.frkl.dev/pypi {opts} {packages}
commands = flake8 src

[coverage:run]
branch = True
source = frkl.project_meta

[coverage:paths]
source =
    src/
    */site-packages/

[coverage:report]
exclude_lines =
    pragma: no cover
   
    def __repr__
    if self\.debug
   
    raise AssertionError
    raise NotImplementedError
   
    if 0:
    if __name__ == .__main__.:

[flake8]
exclude =
    .tox
    build
    dist
    .eggs
    .git
    __pycache__
ignore = F405, W503, E501
max-line-length = 88

[isort]
profile = "black"

[mypy]
mypy_path =
    src/
namespace_packages = false

[mypy-appdirs]
ignore_missing_imports = true

[mypy-asyncclick]
ignore_missing_imports = true

[mypy-deepdiff]
ignore_missing_imports = true

[mypy-devtools]
ignore_missing_imports = true

[mypy-importlib_metadata]
ignore_missing_imports = true

[mypy-mkdocs.*]
ignore_missing_imports = true

[mypy-uvloop]
ignore_missing_imports = true
