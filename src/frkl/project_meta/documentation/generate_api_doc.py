# -*- coding: utf-8 -*-
import importlib
import os
import typing
from pathlib import Path
from types import ModuleType


def gen_pages_for_module(
    module: typing.Union[str, ModuleType], prefix: str = "api_reference"
):

    result = {}
    modules_info = get_source_tree(module)
    for module_name, path in modules_info.items():

        title = module_name
        if title.endswith("__init__"):
            title = title[0:-9]
        if title.endswith("._frkl"):
            continue

        doc_path = f"{prefix}{os.path.sep}{title}.md"
        p = Path("..", path["abs_path"])
        if not p.read_text().strip():
            continue

        result[doc_path] = {
            "python_src": path,
            "content": f"---\ntitle: {title}\n---\n::: {module_name}",
        }

    return result


def get_source_tree(module: typing.Union[str, ModuleType]):

    if isinstance(module, str):
        module = importlib.import_module(module)

    if not isinstance(module, ModuleType):
        raise TypeError(
            f"Invalid type '{type(module)}', input needs to be a string or module."
        )

    module_root = os.path.dirname(module.__file__)
    module_name = module.__name__

    src = {}

    for path in Path(module_root).glob("**/*.py"):

        rel = os.path.relpath(path, module_root)
        mod_name = f"{module_name}.{rel[0:-3].replace(os.path.sep, '.')}"
        rel_path = f"{module_name}{os.path.sep}{rel}"
        src[mod_name] = {"rel_path": rel_path, "abs_path": path}

    return src
