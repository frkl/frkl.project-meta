# -*- coding: utf-8 -*-
def add_devtools_debug_to_globals():
    try:
        from devtools import debug
    except ImportError:
        pass
    else:
        __builtins__["debug"] = debug
