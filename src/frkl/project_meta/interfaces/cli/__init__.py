# -*- coding: utf-8 -*-
import asyncclick as click
import json
import os
import sys
from pathlib import Path
from typing import Optional

import frkl
from frkl.project_meta.core import ProjectMetadata
from frkl.project_meta.documentation.generate_api_doc import gen_pages_for_module
from frkl.project_meta.pyinstaller import PyinstallerBuildRenderer

click.anyio_backend = "asyncio"


@click.group()
@click.pass_context
def cli(ctx):

    pass


def find_unique_main_module_folder(base_dir: Path):

    single_folder = None
    for child in base_dir.iterdir():
        if child.name.endswith("egg-info"):
            continue
        if child.name.startswith(".") or child.name.startswith("_"):
            continue
        if not child.is_dir():
            continue
        if single_folder is not None:
            single_folder = None
            break
        single_folder = child

    if single_folder is None:
        return None

    frkl_module = single_folder / "_frkl" / "__init__.py"
    if frkl_module.exists():
        return single_folder

    return find_unique_main_module_folder(single_folder)


def guess_main_module():

    base_dir = Path(".")
    md_dir = base_dir / ".frkl"
    md_dir.mkdir(parents=True, exist_ok=True)
    md_file = md_dir / "project.json"
    if md_file.exists():

        with open(md_file) as f:
            data = json.load(f)
        main_module = data["main_module"]
    else:
        src_dir = base_dir / "src"
        folder = find_unique_main_module_folder(src_dir)
        if folder is None:
            print(
                "Can't guess project main module, please provide via the '--main-module' option."
            )
            sys.exit(1)

        rel = folder.relative_to(src_dir)
        main_module = rel.as_posix().replace(os.path.sep, ".")

    return main_module


@cli.group()
def doc():
    pass


@doc.command()
def generate_pages():

    gen_pages_for_module(frkl.project_meta, prefix="reference/my_project")


@cli.group()
def info():
    pass


@info.command()
@click.option(
    "--main-module", "-m", required=False, help="the name of the project main module"
)
def update_project_metadata(main_module: str):

    if not main_module:
        main_module = guess_main_module()

    md_obj: ProjectMetadata = ProjectMetadata(project_main_module=main_module)
    md_json = json.dumps(
        md_obj.to_dict(), sort_keys=True, indent=2, separators=(",", ": ")
    )

    base_dir = Path(".")

    md_dir = base_dir / ".frkl"

    md_dir.mkdir(parents=True, exist_ok=True)

    md_file = md_dir / "project.json"

    print(f"Writing metadata to: {md_file.as_posix()}")
    md_file.write_text(md_json)


@info.command()
@click.option(
    "--main-module", "-m", required=False, help="the name of the project main module"
)
@click.pass_context
def metadata(ctx, main_module: str):

    if not main_module:
        main_module = guess_main_module()

    md_obj: ProjectMetadata = ProjectMetadata(project_main_module=main_module)

    md_json = json.dumps(
        md_obj.to_dict(), sort_keys=True, indent=2, separators=(",", ": ")
    )
    print(md_json)


@info.command()
@click.option(
    "--main-module", "-m", required=False, help="the name of the project main module"
)
def runtime_info(main_module: str):

    if not main_module:
        main_module = guess_main_module()

    md_obj: ProjectMetadata = ProjectMetadata(project_main_module=main_module)

    md_json = json.dumps(
        md_obj.runtime_details, sort_keys=True, indent=2, separators=(",", ": ")
    )
    print(md_json)


@info.command()
@click.option(
    "--main-module", "-m", required=False, help="the name of the project main module"
)
@click.argument("path", nargs=1, required=False)
@click.pass_context
def pyinstaller_config(ctx, main_module: str, path: Optional[str] = None):

    if not main_module:
        main_module = guess_main_module()

    if not path:
        path = os.path.join(os.getcwd(), ".frkl", "pyinstaller")

    md_obj: ProjectMetadata = ProjectMetadata(project_main_module=main_module)
    renderer = PyinstallerBuildRenderer(md_obj)
    analysis_args = renderer.create_analysis_args(path)

    md_json = json.dumps(
        analysis_args, sort_keys=True, indent=2, separators=(",", ": ")
    )

    os.makedirs(path, exist_ok=True)

    analysis_args_file = os.path.join(path, "pyinstaller_args.json")

    print(f"Writing pyinstaller config to: {analysis_args_file}")
    with open(analysis_args_file, "w") as f:
        f.write(md_json)


if __name__ == "__main__":
    cli()
